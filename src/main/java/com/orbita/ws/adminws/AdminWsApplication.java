package com.orbita.ws.adminws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminWsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminWsApplication.class, args);
    }

}
