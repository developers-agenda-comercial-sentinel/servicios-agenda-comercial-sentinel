package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Usuario")
public class UsuarioRequest {

    @ApiModelProperty(value = "Código del Usuario", required = true)
    private int codUsuario;

    public int getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(int codUsuario) {
        this.codUsuario = codUsuario;
    }
}
