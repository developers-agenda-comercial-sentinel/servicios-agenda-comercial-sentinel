package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;

public class DashActividadDTO implements Serializable {

    private int codActividad;
    private String actividad;
    private Double porcentaje;

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

}
