package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioUpdateRequest;

public interface SeguridadService {

    ListAnalistaDTO findAll(ListAnalistaRequest request);
    UsuarioDTO findById(UsuarioRequest request);
    BaseResponse update(UsuarioUpdateRequest request);
    OpcionXUsuarioDTO listarPrivilegios(BaseRequest request);

    OpcionDTO execute();
}
