package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.ListAnalistaUbicacionDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;

public interface SeguimientoAnalistaService {

    ListAnalistaUbicacionDTO obtenerUbicacionAnalista(BaseRequest request);
}
