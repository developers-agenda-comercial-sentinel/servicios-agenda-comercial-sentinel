package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.PerfilBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PerfilMapper implements RowMapper<PerfilBean> {

    @Override
    public PerfilBean mapRow(ResultSet rs, int i) throws SQLException {
        PerfilBean bean = new PerfilBean();
        bean.setCodPerfil(rs.getLong("COD_PERFIL"));
        bean.setDescripcion(rs.getString("DESCRIPCION"));
        bean.setEstado(rs.getInt("ESTADO"));

        return bean;
    }
}
