package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.ActividadDAO;
import com.orbita.ws.adminws.model.dao.sp.cartera.SpActividad;
import com.orbita.ws.adminws.model.dto.ListActividadDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActividadDAOImpl implements ActividadDAO {

    @Autowired
    private SpActividad spActividad;

    @Override
    public ListActividadDTO listarActividades() {
        return spActividad.execute();
    }
}
