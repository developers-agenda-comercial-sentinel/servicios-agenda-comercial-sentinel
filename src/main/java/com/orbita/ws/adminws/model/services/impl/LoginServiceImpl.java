package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.LoginDAO;
import com.orbita.ws.adminws.model.dto.LoginDTO;
import com.orbita.ws.adminws.model.dto.request.LoginRequest;
import com.orbita.ws.adminws.model.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    LoginDAO loginDAO;

    @Override
    public LoginDTO getUsuario(LoginRequest usuario) {
        return loginDAO.getUsuario(usuario);
    }
}
