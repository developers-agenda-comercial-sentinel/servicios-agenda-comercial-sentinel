package com.orbita.ws.adminws.model.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orbita.ws.adminws.util.SpConstants;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class ArrayCarteraRequest implements SQLData {

	private int codGestion;
    private int codUsuario;
    private int codCliente;
    private int codBase;

    public int getCodGestion() {
		return codGestion;
	}

	public void setCodGestion(int codGestion) {
		this.codGestion = codGestion;
	}

	public int getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(int codUsuario) {
        this.codUsuario = codUsuario;
    }

    public int getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(int codCliente) {
        this.codCliente = codCliente;
    }

    public int getCodBase() {
        return codBase;
    }

    public void setCodBase(int codBase) {
        this.codBase = codBase;
    }

    @JsonIgnore
    @Override
    public String getSQLTypeName() throws SQLException {
        return SpConstants.TYPE_CARTERA;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
    	codGestion = stream.readInt();
        codUsuario = stream.readInt();
        codCliente = stream.readInt();
        codBase = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
    	stream.writeInt(codGestion);
        stream.writeInt(codUsuario);
        stream.writeInt(codCliente);
        stream.writeInt(codBase);
    }
}
