package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.ReaccionBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReaccionMapper implements RowMapper<ReaccionBean> {

    @Override
    public ReaccionBean mapRow(ResultSet rs, int i) throws SQLException {
        ReaccionBean bean = new ReaccionBean();
        bean.setCodReaccion(rs.getInt("COD_REACCION"));
        bean.setReaccion(rs.getString("REACCION"));
        bean.setCodActividad(rs.getInt("COD_ACTIVIDAD"));
        bean.setActividad(rs.getString("ACTIVIDAD"));
        bean.setMisti(rs.getInt("MISTI"));

        return bean;
    }
}
