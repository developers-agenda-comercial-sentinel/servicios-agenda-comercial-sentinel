package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.ComboDAO;
import com.orbita.ws.adminws.model.dto.ComboDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.services.ComboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComboServiceImpl implements ComboService {

    @Autowired
    private ComboDAO comboDAO;

    @Override
    public ComboDTO cboAgencia() {
        return comboDAO.cboAgencia();
    }

    @Override
    public ComboDTO cboAnalista(BaseRequest request) {
        return comboDAO.cboAnalista(request);
    }
}
