package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.ReporteDAO;
import com.orbita.ws.adminws.model.dao.sp.reporte.SpReporte;
import com.orbita.ws.adminws.model.dao.sp.reporte.SpReporteVisitas;
import com.orbita.ws.adminws.model.dto.ListReporteDTO;
import com.orbita.ws.adminws.model.dto.ListReporteDetalleDTO;
import com.orbita.ws.adminws.model.dto.ListReporteVisitasDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.ReporteDetalleRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReporteDAOImpl implements ReporteDAO {

    @Autowired
    private SpReporte spReporte;

    @Autowired
    private SpReporteVisitas spReporteVisitas;

    @Override
    public ListReporteDTO listarReportes(BaseRequest request) {
        return spReporte.listarReportes(request);
    }

    @Override
    public ListReporteDetalleDTO getReporte(ReporteDetalleRequest request) {
        return spReporte.getReporte(request);
    }

    @Override
    public ListReporteVisitasDTO listarRepVisitasRealizadas(BaseRequest request) {
        return spReporteVisitas.execute(request);
    }
}
