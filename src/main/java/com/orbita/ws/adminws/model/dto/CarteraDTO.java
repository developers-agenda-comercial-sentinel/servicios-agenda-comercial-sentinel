package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.CarteraBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Cartera")
public class CarteraDTO extends BaseResponse implements Serializable {

    private int efectivas;
    private int pendientes;
    private List<CarteraBean> cartera;

    public int getEfectivas() {
        return efectivas;
    }

    public void setEfectivas(int efectivas) {
        this.efectivas = efectivas;
    }

    public int getPendientes() {
        return pendientes;
    }

    public void setPendientes(int pendientes) {
        this.pendientes = pendientes;
    }

    public List<CarteraBean> getCartera() {
        return cartera;
    }

    public void setCartera(List<CarteraBean> cartera) {
        this.cartera = cartera;
    }
}
