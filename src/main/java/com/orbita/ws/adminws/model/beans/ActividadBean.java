package com.orbita.ws.adminws.model.beans;

import com.orbita.ws.adminws.util.SpConstants;
import io.swagger.annotations.ApiModel;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

@ApiModel("BEAN - Actividad")
public class ActividadBean {

    private int codActividad;
    private String actividad;
    private int codColor;

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public int getCodColor() {
        return codColor;
    }

    public void setCodColor(int codColor) {
        this.codColor = codColor;
    }

}
