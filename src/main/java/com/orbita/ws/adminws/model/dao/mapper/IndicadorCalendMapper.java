package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.IndicadorCalendDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IndicadorCalendMapper implements RowMapper<IndicadorCalendDTO> {

    @Override
    public IndicadorCalendDTO mapRow(ResultSet rs, int i) throws SQLException {
        IndicadorCalendDTO dto = new IndicadorCalendDTO();
        dto.setCodColor(rs.getInt("COD_COLOR"));
        dto.setCantidad(rs.getInt("CANT_GESTIONADO"));

        return dto;
    }
}
