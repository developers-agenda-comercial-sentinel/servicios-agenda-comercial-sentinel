package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.ListActividadDTO;

public interface ActividadService {
    ListActividadDTO listarActividades();
}
