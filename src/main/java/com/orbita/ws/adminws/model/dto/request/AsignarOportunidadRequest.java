package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel("REQUEST - Asignar Oportunidad")
public class AsignarOportunidadRequest extends InsRequest implements Serializable {

    @ApiModelProperty(value = "Lista de oportunidades")
    List<ArrayOportunidadRequest> oportunidades;

    public List<ArrayOportunidadRequest> getOportunidades() {
        return oportunidades;
    }

    public void setOportunidades(List<ArrayOportunidadRequest> oportunidades) {
        this.oportunidades = oportunidades;
    }

}
