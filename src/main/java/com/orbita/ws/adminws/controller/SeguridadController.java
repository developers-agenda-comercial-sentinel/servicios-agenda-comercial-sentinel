package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ListAnalistaDTO;
import com.orbita.ws.adminws.model.dto.UsuarioDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.OpcionXUsuarioDTO;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioUpdateRequest;
import com.orbita.ws.adminws.model.services.SeguridadService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Seguridad")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class SeguridadController {

    @Autowired
    private SeguridadService seguridadService;

    @PostMapping(WsConstants.WS_LISTAR_USUARIO)
    public ListAnalistaDTO ListarUsuarios(@RequestBody ListAnalistaRequest request){
        return seguridadService.findAll(request);
    }

    @PostMapping(WsConstants.WS_OBTENER_USUARIO)
    public UsuarioDTO obtenerUsuario(@RequestBody UsuarioRequest request){
        return seguridadService.findById(request);
    }

    @PostMapping(WsConstants.WS_ACTUALIZAR_USUARIO)
    public BaseResponse actualizarUsuario(@RequestBody UsuarioUpdateRequest request){
        return seguridadService.update(request);
    }

    @PostMapping(WsConstants.WS_LISTAR_OPCIONXUSUARIO)
    public OpcionXUsuarioDTO listarPrivilegios(@RequestBody BaseRequest request){
        return seguridadService.listarPrivilegios(request);
    }

}
